extern crate rand;
extern crate num_cpus;

use std::env;
use std::thread;
use std::sync::mpsc::{Sender,Receiver,channel};
use rand::Rng;

#[derive(Debug)]
struct Node {
    //struct representing a PYD/CARD. Stores locations of neighbors as optional numbers.
    //Also keeps track of its own number and whether it is filled or not
    top : Option<usize>,
    bottom : Option<usize>,
    t_left : Option<usize>,
    t_right : Option<usize>,
    l_left : Option<usize>,
    l_right : Option<usize>,
    num : usize,
    fill : bool,
}

impl Node {
    fn new(num : usize) -> Self {
        //Function for creating new nodes with all neighbors blank
        Node{top:None,bottom:None,t_left:None,t_right:None,l_left:None,l_right:None,num,fill:false}
    }
}

fn check_nei(nodes: &Vec<Node>, num: usize) -> Vec<usize> {
    //Function used to check for not filled neighbors
    //Returns a vector with number locations of all empty neighbors
    //For each neighbors, checks it it exists, and if so, then checks if it's filled.
    //A non-filled node is added to vector contain all of them
    let mut p = Vec::new();
    match nodes[num].top {
        Some(n) => {
            if nodes[n].fill == false {
                p.push(n);
            }
        },
        None => (),
    }
    match nodes[num].bottom {
        Some(n) => {
            if nodes[n].fill == false {
                p.push(n);
            }
        },
        None => (),
    }
    match nodes[num].t_left {
        Some(n) => {
            if nodes[n].fill == false {
                p.push(n);
            }
        },
        None => (),
    }
    match nodes[num].t_right {
        Some(n) => {
            if nodes[n].fill == false {
                p.push(n);
            }
        },
        None => (),
    }
    match nodes[num].l_left {
        Some(n) => {
            if nodes[n].fill == false {
                p.push(n);
            }
        },
        None => (),
    }
    match nodes[num].l_right {
        Some(n) => {
            if nodes[n].fill == false {
                p.push(n);
            }
        },
        None => (),
    }
    p
}

fn gen_levels(num_levels : usize, ring_size : usize) -> Vec<Node> {
    //Returns a vector containing connected nodes
    let max_num = num_levels*ring_size;
    let mut nodes = Vec::with_capacity(max_num);
    //The required number of nodes is created, all starting without any connections
    for n in 0..max_num {
        nodes.push(Node::new(n));
    }
    //Following loop connects all the nodes
    for node in nodes.iter_mut() {
        //upper case
        if (node.num >= (max_num-3)) & (node.num <= (max_num-2)) {
            node.t_right = Some(node.num+1)
        } else if node.num == (max_num-4) {
            node.t_left = Some(node.num+1);
            node.t_right = Some(node.num+3);
        } else if node.num != (max_num-1) {
            node.t_left = Some(node.num+1);
            node.t_right = Some(node.num+3);
            node.top = Some(node.num+4);
        }
        //lower case
        if (node.num == 1) | (node.num == 2) {
            node.l_right = Some(node.num-1)
        } else if node.num == 3 {
            node.l_right = Some(node.num-1);
            node.l_left = Some(node.num-3);
        } else if node.num != 0 {
            node.l_left = Some(node.num-3);
            node.l_right = Some(node.num-1);
            node.bottom = Some(node.num-4);
        }
    }
    nodes
}

fn recursive_check(nodes : &Vec<Node>, num : usize, ring_size : usize, length_needed : usize) -> bool {
    //Recursive function that checks if the nodes form a closed "ring"
    //Allows for non complete rings based on the length_needed parameter
    if recursive_check_helper(nodes, num, nodes[num].t_right, ring_size, length_needed - 1) {
        //starts check in t_right neighbor
        return true;
    }
    if recursive_check_helper(nodes, num, nodes[num].l_right, ring_size, length_needed - 1) {
        //starts check in l_right neighbor
        return true;
    }
    if recursive_check_helper(nodes, num, nodes[num].bottom, ring_size, length_needed - 1) {
        //starts check in bottom neighbor
        return true;
    }
    false
}

fn recursive_check_helper(nodes : &Vec<Node>, origin : usize, num : Option<usize>, count : usize, length_needed : usize) -> bool {
    //Helper function for the recusive check
    //nodes is reference to vector containing all of the nodes; origin is the number of starting node;
    //num is the current node; count is the which jump we are on, only a ring_size number of jumps is allowed;
    //length_needed is the number of directly connecting nodes is needed;
    match num {
        //checks if node exists
        Some(num) => {
            if count != 0 {
                if num == origin {
                    //This means that loop is closed. Returns true
                    return true;
                }
                if nodes[num].fill | (length_needed == 0) {
                    //Node has to be filled, or it is past the required number of connecting nodes
                    //After this, it starts another set of recursive functions
                    if recursive_check_helper(nodes, origin, nodes[num].t_right, count - 1, length_needed.saturating_sub(1)) {
                        return true;
                    }
                    if recursive_check_helper(nodes, origin, nodes[num].l_right, count - 1, length_needed.saturating_sub(1)) {
                        return true;
                    }
                    if recursive_check_helper(nodes, origin, nodes[num].bottom, count - 1, length_needed.saturating_sub(1)) {
                        return true;
                    }
                } else {
                }
            }
        },
        None => (),
    }
    //None of the child branches returned true, so false is returned
    false
}

fn sim(repeats : usize, tx : Sender<Vec<usize>>, ring_size : usize, inplane : usize ) -> () {
    //Simulation without a specified end (aka lenght is kept blank)
    //Each run will continue until a "ring" is formed
    //Returns a vector containing the number of nodes needed to complete the "ring"
    let mut results : Vec<usize> = Vec::with_capacity(repeats);
    let mut nodes = gen_levels(240, ring_size);
    let mut rng = rand::thread_rng();
    let middle = nodes.len()/2; //first node is present in the center of the filament
    let mut filled = Vec::new(); //this vector keeps track of filled nodes. Reset each run
    'repeat: for _ in 0..repeats {
        nodes[middle].fill = true; //fills in the first node
        filled.push(nodes[middle].num);
        'outer: loop {
            let mut p = Vec::new();
            for &f in filled.iter() {
                //checks which neighbors to consider. Each node can be present multiple times
                let mut tmp = check_nei(&nodes, f);
                p.append(&mut tmp);
            }
            let n : usize = match rng.choose(&p).cloned() {
                //Randomly picks one from the choices
                Some(n) => n,
                None => {
                    //if there are no valid choices, run is scrapped and reset
                    while let Some(removed) = filled.pop() {
                        nodes[removed].fill = false;
                    }
                    continue 'repeat;
                }
            };
            nodes[n].fill = true; //fills in the chosen node
            filled.push(n);
            for start in filled.iter() {
                //checks if a closed "ring" is present. If so, current run ends and a new one starts
                if recursive_check(&nodes, *start, ring_size, inplane) {
                    break 'outer
                }
            }
        }
        // print_fil(&nodes, &filled); //for debug purposes
        results.push(filled.len()); //Adds the number of nodes needed to result vector
        while let Some(removed) = filled.pop() {
            //reset the run
            nodes[removed].fill = false;
        }
    }
    tx.send(results).unwrap();
}

fn sim_length(repeats : usize, tx : Sender<Vec<bool>>, ring_size : usize, length : usize, inplane : usize ) -> () {
    //very similar to function above. Only new parts are commented
    let mut results : Vec<bool> = Vec::with_capacity(repeats);
    let mut nodes = gen_levels(240, ring_size);
    let mut rng = rand::thread_rng();
    let middle = nodes.len()/2;
    let mut filled = Vec::new();
    'repeat: for _ in 0..repeats {
        nodes[middle].fill = true;
        filled.push(nodes[middle].num);
        for _ in 0..length-1 {
            //This simulation has a specific lenght it will run for, rather than going till completion
            let mut p = Vec::new();
            for &f in filled.iter() {
                let mut tmp = check_nei(&nodes, f);
                p.append(&mut tmp);
            }
            let n : usize = match rng.choose(&p).cloned() {
                Some(n) => n,
                None => {
                    while let Some(removed) = filled.pop() {
                        nodes[removed].fill = false;
                    }
                    continue 'repeat;
                }
            };
            nodes[n].fill = true;
            filled.push(n);
        }
        // print_fil(&nodes, &filled, ring_size); //debug only
        results.push(filled.iter().any(|x| recursive_check(&nodes, *x, ring_size, inplane)));
        //Above line checks whether the filament has a closed "ring". If so appends a success, or failure otherwise
        while let Some(removed) = filled.pop() {
            nodes[removed].fill = false;
        }
    }
    tx.send(results).unwrap();
}

// fn print_fil(nodes : &Vec<Node>, filled : &Vec<usize>, ringsize : usize) -> () {
// //function is for debug purposes only
//     for (n,i) in nodes.iter().enumerate() {
//         if n%ringsize == 0 {
//             print!("\n", );
//         }
//         if i.fill == true {
//             if i.pop == true {
//                 print!("P", );
//             } else {
//                 if n == filled[0] {
//                     print!("S", );
//                 } else {
//                     print!("X", );
//                 }
//             }
//         } else {
//             print!("O", );
//         }
//     }
//     print!("\n", );
// }

fn main() {
    //function the program starts in
    let args : Vec<String> = env::args().collect();
    let mut repeats : usize = 10000;
    let ring_size = 4;
    let mut length : Option<usize> = None;
    let mut inplane : Option<usize> = None;
    for i in &args {
        //program checks for command line parameters
        if i.starts_with("repeats") {
            //sets how many repeats to run. Defaults to 10,000
            let v : Vec<&str> = i.split('=').collect();
            if v.len() == 2 {
                repeats = match v[1].parse() {
                    Ok(n) => n,
                    _ => {
                        println!("Could not parse repeats. Only u32 accepted. Defaulting to 10,000" );
                        10000
                    },
                };
            }
        }
        if i.starts_with("length") {
            //How many nodes (PYD/CARD) to add. If not mentioned, the simulations will run until completion
            let v : Vec<&str> = i.split('=').collect();
            if v.len() == 2 {
                length = match v[1].parse() {
                    Ok(n) => Some(n),
                    _ => panic!("Could not parse length. Only u32 accepted. Value needed if mentioned"),
                };
            } else {
                println!("Length needs a value. Ignoring it in this cycle.");
            }
        }
        if i.starts_with("inplane") {
            //How many nodes have to be connected to be considered a successful run
            let v : Vec<&str> = i.split('=').collect();
            if v.len() == 2 {
                inplane = match v[1].parse() {
                    Ok(n) => Some(n),
                    _ => panic!("Could not parse in-plane. Only u32 accepted. Value needed if mentioned"),
                };
            } else {
                println!("In-plane needs a value. Ignoring it in this cycle.");
            }
        }
        if i.starts_with("help") {
            println!("Usage: pydnode [optinal arguments]", );
            println!("By default, program will output the average number of nodes needed to close a ring within NLRC4 CARD structure.", );
            println!("Optional arguments:", );
            println!("\t length=n", );
            println!("\t Changes mode to run simulation until n-th node is added.\n\t Then it assess if ring is formed or not. \n\t Outputs fraction of simulations with closed rings.", );
            println!("\t inplane=n", );
            println!("\t Setting to choose if all nodes have to be present.\n\t Allows for checking for uncomplete rings with n nodes present.", );
            println!("\t repeats=n", );
            println!("\t Sets number of repeats. Defaults to 10,000.", );
            println!("\t help", );
            println!("\t Prints this text.", );
            return ();
        }
    }
    match inplane {
        //initializes inplane, if not set by command line
        None => inplane = Some(ring_size),
        _ => (),
    }
    match length {
        Some(len) => {
            //set of instructions if running the simulation with a specific number of nodes being added
            //program is multithreaded and will fully use all CPUs
            let (tx, rx): (Sender<Vec<bool>>, Receiver<Vec<bool>>) = channel();
            let mut handles = Vec::new();
            let cpus = num_cpus::get();
            // let cpus = 1; //uncomment to use single thread only.
            let mut left = repeats;
            let mut partial = repeats/cpus;
            for i in 0..cpus {
                left -= partial;
                if (i == cpus-1) & (left != 0) {
                    partial += left;
                }
                let new_tx = tx.clone();
                let handle = thread::spawn(move || sim_length(partial,new_tx,ring_size,len,inplane.unwrap()));
                handles.push(handle);
            }
            let mut results = Vec::with_capacity(repeats);
            for h in handles {
                h.join().unwrap();
                let mut res = rx.recv().unwrap();
                results.append(&mut res);
            }
            let num_fin = results.len();
            println!("{}", results.into_iter().fold(0, |acc,x| acc + (x as u32)) as f32/(num_fin as f32));
            //returns the fraction of filaments (between 0 and 1) that contained a closed "ring"
        },
        None => {
            //set of instructions if running the simulation to figure out the average number of nodes needed to close a "ring"
            //program is multithreaded and will fully use all CPUs
            let (tx, rx): (Sender<Vec<usize>>, Receiver<Vec<usize>>) = channel();
            let mut handles = Vec::new();
            let cpus = num_cpus::get();
            // let cpus = 1; //uncomment to use single thread only.
            let mut left = repeats;
            let mut partial = repeats/cpus;
            for i in 0..cpus {
                left -= partial;
                if (i == cpus-1) & (left != 0) {
                    partial += left;
                }
                let new_tx = tx.clone();
                let handle = thread::spawn(move || sim(partial,new_tx,ring_size,inplane.unwrap()));
                handles.push(handle);
            }
            let mut results = Vec::with_capacity(repeats);
            for h in handles {
                h.join().unwrap();
                let mut res = rx.recv().unwrap();
                results.append(&mut res);
            }
            let num_fin = results.len();
            println!("{}", results.into_iter().fold(0, |acc,x| acc + x) as f32/(num_fin as f32));
            //return the average value of nodes that were needed
        }
    }
}
