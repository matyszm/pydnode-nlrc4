# pydnode NLRC4

Monte Carlo simulation of NLRC4 CARD filament assembly. Based on previously published PYD filament simulation (hence the name pydnode).

## Getting Started

Simply clone the repository and compile with Cargo. See below for more in-depth instructions.

### Prerequisites

Requires Rust with Cargo. Version 1.28.0 was used while making it, but anything above should work too.
Also requires a few Rust crates, but they are listed in the Cargo.toml and will install automatically.
Program only tested on MacOS and Linux systems. However, it should work on windows too.

### Compiling

Clone the repository and compile using "cargo build --release". Executable will be available in target/release folder.

### Usage

There are two ways to use this program. 

1. output the average number of nodes (CARDs) it took to close the ring (this is the default option) 
2. output the fraction of successfully closed rings when N nodes were added (specified with length=N command)

### Optional arguments

* repeats

    Specifies how many times to repeat the simulation. Defaults to 10,000.
    Ex. repeats=1000000

* length

    Specifies number of nodes to add. Changes program to output fraction of successful runs rather than average number of nodes that were needed.
    Ex. length=30
    
* inplane

    Option to change how many nodes have to be in plane to be considered successful. Default is 4, and it should not be set higher.
    Ex. inplane=4
    
* help

    Prints a similar message as this
    
### Example Usage

./pydnode_nlrc4 repeats=10000 length=10

./pydnode_nlrc4 repeats=1000000 inplane=3

./pydnode_nlrc4

Last one will default to 10,000 repeats in average mode.

## License

This project is licensed under the MIT and Apache Licenses - see the [LICENSE-APACHE.md](LICENSE-APACHE.md) and [LICENSE-MIT.md](LICENSE-MIT.md) files for details

## Notes

Code will automatically use all CPUs available. If this is not desired, please uncomment "let cpus = 1" lines in the code and recompile.

Code is based off a previous Python script from my previous publication <http://www.pnas.org/content/115/9/E1963.short>
